Demo Angular 2 Project // shulamit ferber 2017

######

git clone https://gitlab.com/shulamitferber74/ui-demo/


To start development version of project , open cmd at project folder & run : 
-------------

npm install

npm run dev

----

this sets up a gulp watcher to compile ts files & 

opens a http-server that serves index.html at http://localhost:8080 (or other port)


########

To create a production version of project , open cmd at project folder & run : 

---

npm run prod

----
this bundles all the js files from app into a single file (dist/app.js) , copies assets to dist folder & 

opens a http-server that serves dist/index.html at http://localhost:8080 (or other port)


######

folder structure
------------
-index.html - main app file

- dev - ts files

- app - compiled js files

- css

- assets

- dist - distribution folder

- package.json - for npm

- gulpfile - tasks for gulp

