import {Component} from 'angular2/core';

@Component({
    
    selector: 'header-container',
    template: `
        <div class="left-header-area">
            <div id="#menu-toggle" class="icon-wrapper menu-icon" (click)="toggleMenu()"><span id="menu-marker" [hidden]=!menuOpen>&#9666;</span> <img src="assets/icons/menu.svg">
            </div>
            <div class="icon-wrapper">
                <img src="assets/icons/search-01.svg"> </div>
        </div>
        <div class="right-header-area">
            <button class="add-project secondary-color">+ Add project</button>
            <div class="icon-wrapper"><img src="assets/icons/email.svg" style="transform:scale(2)">

            </div>
            <div class="icon-wrapper"><img src="assets/icons/notifications.svg">
            </div>
            <div class="user-image"><img src="assets/icons/profile.png"></div>
        </div>
    `,
})
export class HeaderComponent {

    constructor(){
    
        this.menuOpen=true;
        
    }
    
    
    
    toggleMenu(){
        
        var nav=document.getElementsByClassName("left-nav-container")[0];
        
        var style=window.getComputedStyle(nav);
        
        console.log(style.left)
        if(style.left=="-200px"){
            
            nav.classList.remove('slide-out')
            
            nav.classList.add('slide-in')
        }
        
        else{
            
            nav.classList.remove('slide-in')
            
            nav.classList.add('slide-out')
            
            
            
        
        }
        
        this.menuOpen=!this.menuOpen;
    }

}