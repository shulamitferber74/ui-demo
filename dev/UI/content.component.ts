import {Component} from 'angular2/core';

import {ActivityList} from '../Lists/activity_list.component';

import {TasksList} from '../Lists/tasks_list.component';

import {MessagesList} from '../Lists/messages_list.component';


@Component({
    selector: 'main-content',
    directives:[ActivityList,TasksList,MessagesList],
    template: `
        <div class="content-row">
            <h1>Welcome Shula!</h1>
        </div>
        <div class="content-row">
            <div class="content-row-inner">
                <div class="content-box diagram diagram1"><img src={{diagrams[1]}}></div>
                <div class="content-box diagram diagram2"><img src={{diagrams[2]}}></div>
            </div>
        </div>
        <div class="content-row">
        <div class="content-row-inner">
        <tasks-list class="content-box list"></tasks-list>
        <messages-list class="content-box list" ></messages-list>
        <activity-list class="content-box list" ></activity-list>
            </div>
        </div>
    `
})
export class ContentComponent {

    constructor(){
        
        this.diagrams=[];
        
        this.diagrams[1]='assets/images/mockup_diagrams/diagram1.png';
        
        this.diagrams[2]='assets/images/mockup_diagrams/diagram2.png';
        
       
    
    }
    


}