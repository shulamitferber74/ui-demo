import {Component} from 'angular2/core';

@Component({
    
    selector: 'menu-items',
    template: `
        <ul class="left-nav">
            <li *ngFor="#item of menuItems" >
            <img src={{item.image}}>{{item.name}}</li>
        </ul>
    `,
})
export class MenuComponent {

    constructor(){
    this.menuItems=[{name:'Home' ,image:'assets/icons/home.svg'},
                    {name:'Workflow' , image:'assets/icons/workflow-01.svg'},
                    {name:'Statistics' , image:'assets/icons/statistics.svg'},
                    {name:'Calendar' , image:'assets/icons/calendar.svg'},
                    {name:'Users' , image:'assets/icons/users.svg'},
       {name:'Setting' , image:'assets/icons/setting.svg'}
    ]
    
    }

}