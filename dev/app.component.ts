import {Component} from 'angular2/core';
import {MenuComponent} from './UI/menu.component';
import {HeaderComponent} from './UI/header.component';
import {ContentComponent} from './UI/content.component';



@Component({
    
    selector: 'my-app',
    directives:[MenuComponent,HeaderComponent,ContentComponent],
    template:`<div id="navigation" class='left-nav-container'>
    <div class="top-logo"><img src="assets/icons/logo.svg" width="130px"></div>
       <menu-items> 
    </menu-items>
    </div>
<header-container class="header bottom-border">
</header-container>
<main-content class="content-container">
</main-content>`
})

export class AppComponent {

}