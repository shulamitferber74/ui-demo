import {Component, Input} from 'angular2/core';

@Component({
    
    selector: 'message-item',
    template: `<li class="bottom-border">                        
                            <div class="list-item-left">
                                <img class="circle-image" src={{image}}/><div class="list-item-text"><span class="first-row">{{name}}
                                <span class="first-row-smaller">{{time}}</span></span>
                                <div class="second-row">{{text}}</div>
                                 <div class="third-row"><img src="assets/icons/share.svg"><img src="assets/icons/gear.svg"></div>
                                </div></div>
                            <div class="list-item-right"><img src="assets/icons/list-more.svg"></div>
                        </li>
                
    `
})
export class MessageItem {

    @Input() name:String;
    
    @Input() text:String;
    
    @Input() image:String;
    
    @Input() time:String;
    
    constructor(){
        

}}