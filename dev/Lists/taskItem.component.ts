import {Component, Input} from 'angular2/core';

@Component({
    
    selector: 'task-item',
    template: `<li class="bottom-border" >
                            <div class="list-item-left">
                                <div class="circle">{{name}}</div><div class="list-item-text"><span class="first-row">{{text}}</span>
                                <span class="second-row highlighted"> <img src="assets/icons/timer.svg">{{time}}</span></div></div>
                            <div class="list-item-right"><img src="assets/icons/list-more.svg"></div>
                        </li>`,
})
export class TaskItem {

    @Input() name:String;
    
    @Input() text:String;
    
    @Input() time:String;
    
    
    constructor(){
        
        

}

}