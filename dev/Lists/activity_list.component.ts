import {Component, Input} from 'angular2/core';
import {ActivityItem} from './activityItem.component';

@Component({
     
    selector: 'activity-list',
    directives:[ActivityItem],
    template: `
           <div class="list-header bottom-border">
                        <div class="list-header-left">Activity</div>
                        <div class="list-header-right">
                            <div class="list-header-item circle">2</div>
                        </div>
                    </div>
                    <ul>
                        
                        <activity-item *ngFor="#activity of activity_list"
                            [image]=activity.image
                            [name]=activity.name
                            [text]=activity.text
                            [place]=activity.place
                            [time]=activity.time>                        
                        </activity-item>
                </ul>
    `,
})
export class ActivityList {

    constructor(){
        
        this.activity_list=[{
           "name":"Nina Jones",
           "image":"assets/images/photo1.png",
           "text":"added a new project",
            "place":'Free UI kits',
           "time":"5 days ago"
           
       },
                      {
           "name":"Nina Jones",
            "image":"assets/images/photo2.png",
           "text":"commented on photo",
            "place":'Free UI kits',
           "time":"2 days ago"
           
       },
                       
                       {
           "name":"Alex Cloony",
            "image":"assets/images/photo3.png",
           "text":"completed task",
                           "place":'Free UI kits',
           "time":"5 days ago"
           
       },
                       {
           "name":"Alexandra spears",
            "image":"assets/images/photo4.png",
           "text":"added a new project",
            "place":'Free UI kits',
           "time":"10 days ago"
           
       }
                      ]
    }

}