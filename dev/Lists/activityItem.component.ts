import {Component, Input} from 'angular2/core';

@Component({
     
    selector: 'activity-item',
    template: ` <li class="bottom-border" >
                            <div class="list-item-left">
                                <div><img class="circle-image" src={{image}}></div>
                                <div class="list-item-text">
                                <span class="first-row">{{name}}
                                <span class="first-row-smaller">{{text}}</span>{{place}}</span>
                                <div class="second-row"><img src="assets/icons/timer.svg">{{time}}</div>
                                 </div>
                    </div>
                    </li>`,
})
export class ActivityItem {

    @Input() name:String;
    
     @Input() image:String;
    
    @Input() text:String;
    
    @Input() place:String;
    
    @Input() time:String;
    
    constructor(){
        
      
           
       
    }

}