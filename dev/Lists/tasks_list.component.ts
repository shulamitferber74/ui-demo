import {Component} from 'angular2/core';
import {TaskItem} from './taskItem.component';

@Component({
    
    selector: 'tasks-list',
     directives:[TaskItem],
    template: `
            
                    <div class="list-header bottom-border">
                        <div class="list-header-left">Tasks</div>
                        <div class="list-header-right">
                            <div class="list-header-item circle circle-second">5</div>
                            <div class="list-header-item circle">2</div>
                        </div>
                    </div>
                    <ul>
                        <task-item *ngFor="#task of taskList" 
                        [name]=task.name 
                        [text]=task.text 
                        [time]=task.time>
                    </task-item>
                    </ul>
               
    `
})
export class TasksList {

    constructor(){
        
this.taskList=[{
           "name":"N",
           "text":"New website from semyu",
           "time":"5 days ago"
           
       },
                      {
           "name":"F",
           "text":"Free Business PSD Template",
           "time":"2 days ago"
           
       },
                       
                       {
           "name":"N",
           "text":"New Logo for JCD",
           "time":"5 days ago"
           
       },
                       {
           "name":"F",
           "text":"Free Icons Set",
           "time":"10 days ago"
           
       },
               
    }

}