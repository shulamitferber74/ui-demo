import {Component} from 'angular2/core';
import {MessageItem} from './messagesItem.component';

@Component({
    
    selector: 'messages-list',
    directives:[MessageItem],
    template: `
            <div class="list-header bottom-border">
                        <div class="list-header-left">Messages</div>
                        <div class="list-header-right">
                            <div class="list-header-item circle">2</div>
                        </div>
                    </div>
                    <ul>
                        
                        <message-item *ngFor="#message of messageList" 
                        [name]=message.name 
                        [text]=message.text 
                        [time]=message.time
                        [image]=message.image
                            >                        
                        </message-item>
                </ul>
    `
})
export class MessagesList {

    constructor(){
        
this.messageList=[{
           "sender":"Nina Jones",
           "image":"assets/images/photo1.png",
           "text":"Hey ! Its Me Again! [...]",
           "time":"5 days ago"
           
       },
                      {
           "sender":"Nina Jones",
           "image":"assets/images/photo1.png",
           "text":"Hey ! I attached Some PSD [...]",
           "time":"2 days ago"
           
       },
                       
                       {
           "sender":"James Smith",
            "image":"assets/images/photo2.png",
           "text":"Good Morning",
           "time":"5 days ago"
           
       },
                       {
           "sender":"Nina Jones",
            "image":"assets/images/photo1.png",
           "text":"Bring Me the coffee",
           "time":"10 days ago"
           
       }
                      ]

}}