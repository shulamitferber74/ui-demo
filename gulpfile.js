var gulp = require('gulp');

var concat = require('gulp-concat');

var path = require("path");
     
var TS_FILES_LOC = 'dev/'; //input typescript files

var DEV_JS_LOC = 'app/'; // output js files for dev -- not concated

var PROD_LOC = 'dist/'; // outpput for final app.js prod file

/* JS & TS */
var jsuglify = require('gulp-uglify');

var typescript = require('gulp-typescript');

var tsProject = typescript.createProject('tsconfig.json'); // config for dev compile

var tsProject_Prod = typescript.createProject('tsconfig.json',{ typescript: require('typescript'),
  outFile: 'app/app.js'}); // config for prod compile

//build ts into js separate files - for dev
gulp.task('build-ts', function () {
    return gulp.src(TS_FILES_LOC + '**/*.ts')
        .pipe(typescript(tsProject))
       /* .pipe(jsuglify())*/
        .pipe(gulp.dest(DEV_JS_LOC));
});

//build ts into single file - for prod

gulp.task('build-ts-prod', function () {
    return gulp.src(TS_FILES_LOC + '**/*.ts')
        .pipe(typescript(tsProject_Prod))
       /* .pipe(jsuglify())*/
        .pipe(gulp.dest(PROD_LOC));
});

//copy images///
gulp.task('copy-assets', function () {
    return gulp.src('assets/**/*.{png,json,svg,jpg}')
        // Perform minification tasks, etc here
        .pipe(gulp.dest('dist/assets/'));

});


//copy stylesheets//
gulp.task('copy-stylesheets', function () {
    return gulp.src('css/**/*.{css,woff}')
        // Perform minification tasks, etc here
        .pipe(gulp.dest('dist/css/'));

    ;
});


gulp.task('watch', function () {
    gulp.watch(TS_FILES_LOC + '**/*.ts', ['build-ts']);
});

gulp.task('default', ['watch', 'build-ts']);

gulp.task('prod', ['build-ts-prod','copy-assets','copy-stylesheets']);